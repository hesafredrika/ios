//
//  SettingsStorage.swift
//  Fredrika
//
//  Created by Alexander Simson on 2017-02-20.
//  Copyright © 2017 FR. All rights reserved.
//

import UIKit

class SettingsStorage {
    
    private static let areaOfInterestKey = "areaOfInterest"
    private static let volunteeringKey = "volunteering"
    
    static func areasOfInterest() -> Array<AreaOfInterest> {
        let savedPlaces = UserDefaults.standard.data(forKey: areaOfInterestKey)
        var places = Array<AreaOfInterest>()
        
        if savedPlaces != nil {
            places = NSKeyedUnarchiver.unarchiveObject(with: savedPlaces!) as! Array<AreaOfInterest>
        }
        return places
    }
    
    static func add(areaOfInterest: AreaOfInterest) {
        var areas = SettingsStorage.areasOfInterest()
        areas.append(areaOfInterest)
        
        let data = NSKeyedArchiver.archivedData(withRootObject:areas)
        UserDefaults.standard.set(data, forKey: areaOfInterestKey)
        UserDefaults.standard.synchronize()
    }
    
    static func remove(areaOfInterest: AreaOfInterest) {
        var areas = SettingsStorage.areasOfInterest()
        if let index = areas.index(where: { $0 == areaOfInterest }) {
            areas.remove(at: index)
            
            let data = NSKeyedArchiver.archivedData(withRootObject:areas)
            UserDefaults.standard.set(data, forKey: areaOfInterestKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    static func volunteeringOptions() -> Array<Volunteering> {
        let savedOptions = UserDefaults.standard.data(forKey: volunteeringKey)
        var options = Array<Volunteering>()
        
        if savedOptions != nil {
            options = NSKeyedUnarchiver.unarchiveObject(with: savedOptions!) as! Array<Volunteering>
        }
        return options
    }
    
    static func add(volunteering: Volunteering) {
        var options = SettingsStorage.volunteeringOptions()
        options.append(volunteering)
        
        let data = NSKeyedArchiver.archivedData(withRootObject:options)
        UserDefaults.standard.set(data, forKey: volunteeringKey)
        UserDefaults.standard.synchronize()
    }
    
    static func remove(volunteering: Volunteering) {
        var areas = SettingsStorage.volunteeringOptions()
        if let index = areas.index(where: { $0 == volunteering }) {
            areas.remove(at: index)
            
            let data = NSKeyedArchiver.archivedData(withRootObject:areas)
            UserDefaults.standard.set(data, forKey: volunteeringKey)
            UserDefaults.standard.synchronize()
        }
    }
}
