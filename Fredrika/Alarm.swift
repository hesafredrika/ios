//
//  Alarm.swift
//  Fredrika
//
//  Created by Alexander Simson on 2016-12-07.
//  Copyright © 2016 FR. All rights reserved.
//

import UIKit

import MapKit

class Alarm: NSObject {

    var id : Int?
    var active : Bool = false
    var subject : String?
    var message : String?
    var url : String?
    var updated : Date?
    var boundingRect: MKMapRect?
    
    required init(withPayload payload: Dictionary<AnyHashable, Any>) {
        let subject = payload["subject"] as? String
        let active = payload["active"] as! Bool
        let id = payload["id"] as! Int
        let message = payload["message"] as? String
        let url = payload["url"] as? String
        let timestamp = payload["updated"] as? String
        
        struct TimestampDateFormatter {
            static let formatter: DateFormatter = {
                let formatter = DateFormatter()
                formatter.dateFormat = "YYMMdd'T'HHmmss"
                return formatter
            }()
        }
        
        self.subject = subject
        self.active = active
        self.id = id
        self.message = message
        self.url = url
        
        if let timestamp = timestamp {
            self.updated = TimestampDateFormatter.formatter.date(from: timestamp)
        }
        
        // Create bounding box rect
        if let locations = payload["location"] as? Array<Dictionary<AnyHashable, Any>> {
            if let coordinates = locations[0]["boundingbox"] as? Array<String> {
                let top = CLLocationCoordinate2DMake(Double(coordinates[1])!, Double(coordinates[3])!)
                let bottom = CLLocationCoordinate2DMake(Double(coordinates[0])!, Double(coordinates[2])!)
                
                // Alarm bounding box map rect
                let topLeft = MKMapPointForCoordinate(top)
                let bottomRight = MKMapPointForCoordinate(bottom)
                self.boundingRect = MKMapRectMake(fmin(topLeft.x,bottomRight.x), fmin(topLeft.y,bottomRight.y), fabs(topLeft.x-bottomRight.x), fabs(topLeft.y-bottomRight.y))
            }
        }
    }
    
    func intersects(area: AreaOfInterest) -> Bool {
        guard area.location != nil && self.boundingRect != nil else {
            print("Invalid location")
            return false
        }
        
        let center = area.location!.coordinate
        let circle = MKCircle(center: center, radius: Double(area.radius))
        let areaRect = circle.boundingMapRect
        
        // Check if the area bounding rect intersects with the bounding rect from the API
        if MKMapRectIntersectsRect(areaRect, self.boundingRect!) {
            print("Found an intersecting area: \(area.name!)")
            return true
        }
        
        return false
    }
    
    func intersects(location: CLLocation?) -> Bool {
        guard location != nil && self.boundingRect != nil else {
            print("Invalid location")
            return false
        }
        
        let userPoint = MKMapPointForCoordinate(location!.coordinate)
        return MKMapRectContainsPoint(self.boundingRect!, userPoint)
    }
    
    func messageUrl() -> URL? {
        if let id = self.id {
            let urlString = "https://hesafredrika.se/message/\(id)"
            return URL(string: urlString)!
        }
        return nil
    }
    
}
