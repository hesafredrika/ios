//
//  Notification.swift
//  Fredrika
//
//  Created by Alexander Simson on 2017-07-26.
//  Copyright © 2017 FR. All rights reserved.
//

import UIKit

import MapKit
import UserNotifications

class Notification: NSObject {
    
    var latitude: Double?
    var longitude: Double?
    var boundingRect: MKMapRect?
    var locationName: String?
    var placeId: Int?
    var message: String?
    var title: String?
    
    required init(withPayload payload: Dictionary<AnyHashable, Any>) {
        // Parse location, place and message
        self.latitude = payload["lat"] as? Double
        self.longitude = payload["lon"] as? Double
        self.locationName = payload["location_name"] as? String
        self.placeId = payload["place_id"] as? Int
        self.message = payload["message"] as? String
        self.title = payload["message_subject"] as? String
        
        // Create bounding box rect
        if let location = payload["location"] as? String {
            let coordinates = location.replacingOccurrences(of: "[\"", with: "").replacingOccurrences(of: "\"]", with: "").components(separatedBy: "\",\"")
            let top = CLLocationCoordinate2DMake(Double(coordinates[1])!, Double(coordinates[3])!)
            let bottom = CLLocationCoordinate2DMake(Double(coordinates[0])!, Double(coordinates[2])!)
            
            // Alarm bounding box map rect
            let topLeft = MKMapPointForCoordinate(top)
            let bottomRight = MKMapPointForCoordinate(bottom)
            self.boundingRect = MKMapRectMake(fmin(topLeft.x,bottomRight.x), fmin(topLeft.y,bottomRight.y), fabs(topLeft.x-bottomRight.x), fabs(topLeft.y-bottomRight.y))
        }
    }
    
    func intersects(area: AreaOfInterest) -> Bool {
        guard area.location != nil && self.boundingRect != nil else {
            print("Invalid location")
            return false
        }
        
        let center = area.location!.coordinate
        let circle = MKCircle(center: center, radius: Double(area.radius))
        let areaRect = circle.boundingMapRect
        
        // Check if the area bounding rect intersects with the bounding rect from the API
        if MKMapRectIntersectsRect(areaRect, self.boundingRect!) {
            print("Found an intersecting area: \(area.name!)")
            return true
        }
        
        return false
    }
    
    func intersects(location: CLLocation?) -> Bool {
        guard location != nil && self.boundingRect != nil else {
            print("Invalid location")
            return false
        }
        
        let userPoint = MKMapPointForCoordinate(location!.coordinate)
        return MKMapRectContainsPoint(self.boundingRect!, userPoint)
    }
    
    func scheduleLocalNotification() {
        // Make sure there's a title and a message
        if self.title != nil && self.message != nil {
            // Base64 decode the message
            if let message = self.message?.base64Decoded() {
                // Create the local notification
                let content = UNMutableNotificationContent()
                content.title = NSString.localizedUserNotificationString(forKey: self.title!, arguments: nil)
                content.body = NSString.localizedUserNotificationString(forKey: message, arguments: nil)
                content.sound = UNNotificationSound.default()
                
                let request = UNNotificationRequest(identifier: "Alarm", content: content, trigger: nil)
                
                let center = UNUserNotificationCenter.current()
                center.add(request) { (error : Error?) in
                    if let theError = error {
                        print(theError.localizedDescription)
                    }
                }
            }
        }
    }
}
