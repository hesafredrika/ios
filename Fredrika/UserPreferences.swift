//
//  UserPreferences.swift
//  Fredrika
//
//  Created by Alexander Simson on 2017-07-26.
//  Copyright © 2017 FR. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    private static let currentLocationKey = "matchCurrentLocation"
    private static let receiveMessageKey = "receiveMessages"
    
    var matchWithCurrentLocationEnabled: Bool {
        get {
            return self.bool(forKey: UserDefaults.currentLocationKey)
        }
        set {
            self.set(newValue, forKey: UserDefaults.currentLocationKey)
        }
    }
    
    var receiveMessagesEnabled: Bool {
        get {
            return self.bool(forKey: UserDefaults.receiveMessageKey)
        }
        set {
            self.set(newValue, forKey: UserDefaults.receiveMessageKey)
        }
    }
    
    static func registerDefaults() {
        UserDefaults.standard.register(defaults: [UserDefaults.currentLocationKey : true,
                                                  UserDefaults.receiveMessageKey : true])
    }
}
