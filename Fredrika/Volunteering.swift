//
//  Volunteering.swift
//  Fredrika
//
//  Created by Alexander Simson on 2016-01-18.
//  Copyright © 2016 FR. All rights reserved.
//

import UIKit

class Volunteering: NSObject, NSCoding {
    var name: String?
    var id: Int
    var created: Date?
    var url: URL?
    var note: String?
    var selected: Bool = false
    
    required init(withPayload payload: Dictionary<AnyHashable, Any>) {
        let name = payload["name"] as! String
        let id = payload["id"] as! Int
        let created = payload["created"] as! String
        let url = payload["url"] as? String
        let note = payload["note"] as? String
        
        struct TimestampDateFormatter {
            static let formatter: DateFormatter = {
                let formatter = DateFormatter()
                formatter.dateFormat = "YYMMdd'T'HHmmss"
                return formatter
            }()
        }
        
        self.name = name
        self.id = id
        self.note = note
        self.selected = false
        
        if let urlString = url {
            self.url = URL.init(string: urlString)
        }
        
        self.created = TimestampDateFormatter.formatter.date(from: created)
    }
    
    required init(coder decoder: NSCoder) {
        self.name = decoder.decodeObject(forKey: "name") as? String
        self.id = decoder.decodeInteger(forKey: "id")
        self.note = decoder.decodeObject(forKey: "note") as? String
        self.selected = decoder.decodeBool(forKey: "selected")
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(name, forKey: "name")
        coder.encode(id, forKey: "id")
        coder.encode(note, forKey: "note")
        coder.encode(selected, forKey: "selected")
    }
    
    static func == (lhs: Volunteering, rhs: Volunteering) -> Bool {
        return (lhs.id == rhs.id)
    }
    
    override var debugDescription : String {
        return "Volunteering: \(String(describing: self.name)), id: \(self.id)"
    }
}
