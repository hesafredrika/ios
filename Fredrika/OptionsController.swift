//
//  OptionsController.swift
//  Fredrika
//
//  Created by Alexander Simson on 2016-01-14.
//  Copyright © 2016 FR. All rights reserved.
//

import UIKit

class OptionsController: UITableViewController {
    
    var objects: Array<Volunteering> = Array<Volunteering>()
    
    init(withObjects objects: Array<Volunteering>) {
        super.init(style: UITableViewStyle.grouped)
        self.objects = objects
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.objects.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath)

        let option: Volunteering = self.objects[indexPath.row]
        cell.textLabel?.text = option.name
        cell.accessoryType = option.selected ? UITableViewCellAccessoryType.checkmark : UITableViewCellAccessoryType.none;
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let option: Volunteering = self.objects[indexPath.row]
        option.selected = !option.selected
        
        if option.selected {
            SettingsStorage.add(volunteering: option)
        } else {
            SettingsStorage.remove(volunteering: option)
        }

        tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
