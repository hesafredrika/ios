//
//  SelectLocationController.swift
//  Fredrika
//
//  Created by Alexander Simson on 2016-12-09.
//  Copyright © 2016 FR. All rights reserved.
//

import UIKit
import MapKit

class Annotation : NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }
    
}

protocol SelectLocationControllerDelegate {
    func didSelect(area: AreaOfInterest, controller: SelectLocationController)
}

class SelectLocationController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet var mapView: MKMapView!
    @IBOutlet var slider: UISlider!
    @IBOutlet var sizeLabel: UILabel!
    
    var userLocation: CLLocation?
    let locationManager: CLLocationManager = CLLocationManager()
    let authorizationStatus = CLLocationManager.authorizationStatus()
    var radius: Float = 2000
    var delegate: SelectLocationControllerDelegate?
    
    // MARK: - View Controller
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = kCLLocationAccuracyKilometer
        
        if authorizationStatus == .denied || authorizationStatus == .notDetermined {
            self.locationManager.requestWhenInUseAuthorization()
        }
        
        let longPressRecognizer = UILongPressGestureRecognizer.init(target: self, action: #selector(self.longPressAction))
        self.mapView.addGestureRecognizer(longPressRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.locationManager.startUpdatingLocation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.locationManager.stopUpdatingLocation()
    }
    
    // MARK: - Actions
    
    func longPressAction(_ recognizer: UILongPressGestureRecognizer) {
        guard recognizer.state == .began else {
            return
        }
        
        let point = recognizer.location(in: self.mapView)
        let coordinate = self.mapView.convert(point, toCoordinateFrom: self.mapView)
        self.addAnnotation(coordinate: coordinate, center: false)
    }
    
    @IBAction func sliderValueChanged(_ sender: Any) {
        let newRadius = roundf(slider.value)
        if self.radius != newRadius {
            self.radius = newRadius * 50.0
            slider.value = newRadius
            
            if let circle = self.mapView.overlays.first as? MKCircle {
                self.mapView.remove(circle)
                let newCircle = MKCircle.init(center: circle.coordinate, radius: Double(self.radius))
                self.mapView.add(newCircle)
            }
            
            self.sizeLabel.text = NSLocalizedString("Storlek på område: \(self.slider.value * 100.0) m", comment:"")
        }
    }

    @IBAction func save(_ sender: Any) {
        if let annotation = self.mapView.annotations.first {
            let location = CLLocation.init(latitude: annotation.coordinate.latitude, longitude: annotation.coordinate.longitude)
            let area = AreaOfInterest.init(withLocation: location, radius: self.radius, name: NSLocalizedString("Okänt område", comment: ""))
            self.saveAreaOfInterest(area: area)
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    // MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            if (self.userLocation == nil) {
                let radius = 5000.0
                let coordinateRegion = MKCoordinateRegionMakeWithDistance( location.coordinate, radius, radius )
                self.mapView.setRegion(coordinateRegion, animated: true)
            }
            self.userLocation = location
        }
    }
    
    // MARK: - MKMapViewDelegate
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let pinAnnotationView = MKPinAnnotationView.init(annotation: annotation, reuseIdentifier: "annotation")
        pinAnnotationView.pinTintColor = self.view.tintColor
        pinAnnotationView.isDraggable = true
        pinAnnotationView.animatesDrop = true
        return pinAnnotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, didChange newState: MKAnnotationViewDragState, fromOldState oldState: MKAnnotationViewDragState) {
        if newState == .ending {
            if let circle = self.mapView.overlays.first as? MKCircle {
                self.mapView.remove(circle)
                let newCircle = MKCircle.init(center: circle.coordinate, radius: Double(self.radius))
                self.mapView.add(newCircle)
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let overlay = overlay as? MKCircle {
            let circleRenderer = MKCircleRenderer(circle: overlay)
            circleRenderer.fillColor = UIColor.blue.withAlphaComponent(0.1)
            circleRenderer.strokeColor = UIColor.blue
            circleRenderer.lineWidth = 1.0
            return circleRenderer
        }
        return MKOverlayRenderer()
    }
    
    // MARK: Private
    
    private func saveAreaOfInterest(area: AreaOfInterest){
        if let location = area.location {
            CLGeocoder().reverseGeocodeLocation(location, completionHandler:
                {(placemarks, error) in
                    if error != nil {
                        print("reverse geodcode fail: \(error!.localizedDescription)")
                    }
                    
                    if placemarks  != nil {
                        if let placemark: CLPlacemark = placemarks!.first {
                            if placemark.locality != nil && placemark.subLocality != nil {
                                area.name = "\(placemark.subLocality!), \(placemark.locality!)  / \(area.radius) m"
                            } else {
                                area.name = "\(placemark.locality!) / \(area.radius) m"
                            }
                        }
                    }
                    
                    SettingsStorage.add(areaOfInterest: area)
                    
                    if let delegate = self.delegate {
                        delegate.didSelect(area: area, controller: self)
                    }
            })
        }
    }
    
    private func addAnnotation(coordinate: CLLocationCoordinate2D, center: Bool) {
        self.mapView.removeAnnotations(self.mapView.annotations)
        let annotation = Annotation.init(coordinate: coordinate)
        self.mapView.addAnnotation(annotation)
        
        self.mapView.removeOverlays(self.mapView.overlays)
        let circle = MKCircle.init(center: coordinate, radius: Double(self.radius))
        self.mapView.add(circle)
        
        if center {
            self.mapView.setCenter(coordinate, animated: true)
            self.mapView.setRegion(MKCoordinateRegion.init(center: coordinate, span: MKCoordinateSpan.init(latitudeDelta: 0.1, longitudeDelta: 0.1)), animated: true)
        }
    }
}
