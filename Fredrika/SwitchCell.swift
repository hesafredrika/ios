//
//  SwitchCell.swift
//  Fredrika
//
//  Created by Alexander Simson on 2016-01-14.
//  Copyright © 2016 FR. All rights reserved.
//

import UIKit

class SwitchCell: UITableViewCell {

    @IBOutlet var enableSwitch: UISwitch!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!

}
