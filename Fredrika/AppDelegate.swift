//
//  AppDelegate.swift
//  Fredrika
//
//  Created by Alexander Simson on 2015-12-22.
//  Copyright © 2015 FR. All rights reserved.
//

import UIKit

import MapKit
import Firebase
import UserNotifications
import Alamofire

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    let locationManager: CLLocationManager = CLLocationManager()
    let authorizationStatus = CLLocationManager.authorizationStatus()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        FirebaseApp.configure()
        
        Alamofire.request("https://hesafredrika.se/api/1.0/topics")
            .responseJSON { response in
                if let responseArray = response.result.value as? NSArray {
                    for object in responseArray {
                        if let topic = object as? NSDictionary {
                            if let name = topic["name"] as? String,
                                let id = topic["id"] as? Int
                            {
                                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                                    print("Subscribe to topic \(name) (\(id))")
                                    Messaging.messaging().subscribe(toTopic: "/topics/\(id)")
                                })
                            }
                        }
                    }
                }
        }
        
        Messaging.messaging().delegate = self
        
        // Setup default user settings
        UserDefaults.registerDefaults()
        
        // Setup the location manager
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = kCLLocationAccuracyKilometer
        
        if authorizationStatus == .denied || authorizationStatus == .notDetermined {
            self.locationManager.requestWhenInUseAuthorization()
        }
        
        locationManager.startUpdatingLocation()
        
        return true
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        self.locationManager.stopUpdatingLocation()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        self.locationManager.startUpdatingLocation()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // Print full message.
        print(userInfo)
        
        locationManager.startUpdatingLocation()
        
        // Create notification model object from push notification payload
        let notification = Notification(withPayload: userInfo)
        
        // See if messages are disabled by the user
        if !UserDefaults.standard.receiveMessagesEnabled {
            print("Messages was disabled by the user")
            completionHandler(UIBackgroundFetchResult.noData)
            return
        }
        
        // Match with the current location if enabled by the user
        if UserDefaults.standard.matchWithCurrentLocationEnabled {
            print("Will try to match the notification with the users current location")
            if let location = self.locationManager.location {
                if notification.intersects(location: location) {
                    print("Notification area contains the users current location")
                    self.presentNotification(notification: notification)
                    completionHandler(UIBackgroundFetchResult.newData)
                    return
                }
            }
        }
        
        // Go through all the users areas of interest to determine if the alarm is relevant
        let areas = SettingsStorage.areasOfInterest()
        for area in areas {
            if notification.intersects(area: area) {
                self.presentNotification(notification: notification)
                completionHandler(UIBackgroundFetchResult.newData)
                return
            }
        }
        
        completionHandler(UIBackgroundFetchResult.noData)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // MARK: - Private
    
    private func presentNotification(notification: Notification) {
        // Show an alert if the application is active
        if UIApplication.shared.applicationState == .active {
            if let title = notification.title, let message = notification.message?.base64Decoded() {
                let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
                self.window?.rootViewController?.present(alert, animated: true, completion: nil)
            }
            // If the application is in the background: schedule a local notification
        } else {
            notification.scheduleLocalNotification()
        }
    }
    
    // MARK: - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("Updated location")
    }
}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        // Print full message.
        print(userInfo)
        
        completionHandler()
    }
}

extension AppDelegate : MessagingDelegate {
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
    }
}
