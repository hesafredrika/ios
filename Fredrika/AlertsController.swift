//
//  AlertsController.swift
//  Fredrika
//
//  Created by Alexander Simson on 2016-12-06.
//  Copyright © 2016 FR. All rights reserved.
//

import UIKit
import Alamofire
import SafariServices
import CoreLocation

class AlertsController: UITableViewController {

    var active: Array<Alarm> = Array<Alarm>()
    var inactive: Array<Alarm> = Array<Alarm>()
    
    let locationManager: CLLocationManager = CLLocationManager()
    let authorizationStatus = CLLocationManager.authorizationStatus()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backButtonItem: UIBarButtonItem = UIBarButtonItem.init(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil);
        self.navigationItem.backBarButtonItem = backButtonItem;
        
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.estimatedRowHeight = 60.0;
        
        // Setup the location manager
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = kCLLocationAccuracyKilometer
        
        if authorizationStatus == .denied || authorizationStatus == .notDetermined {
            self.locationManager.requestWhenInUseAuthorization()
        }
        
        self.locationManager.startUpdatingLocation()
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: #selector(self.loadAlarms), for: .valueChanged)
        
        self.loadAlarms()
    }
    
    // MARK: - Private
    
    func loadAlarms() {
        self.refreshControl?.beginRefreshing()
        
        Alamofire.request("https://hesafredrika.se/api/1.0/alarms")
            .responseJSON { response in
                if let responseArray = response.result.value as? NSArray {
                    var alarms: Array<Alarm> = Array<Alarm>()
                    
                    for object in responseArray {
                        if let alarm = object as? Dictionary<AnyHashable, Any> {
                            let model = Alarm(withPayload: alarm)
                            alarms.append(model)
                        }
                    }
                    
                    alarms = alarms.filter({ (alarm) -> Bool in
                        // Match with the current location if enabled by the user
                        if UserDefaults.standard.matchWithCurrentLocationEnabled {
                            if let location = self.locationManager.location {
                                if alarm.intersects(location: location) {
                                    return true
                                }
                            }
                        }
                        
                        // Go through all the users areas of interest to determine if the alarm is relevant
                        let areas = SettingsStorage.areasOfInterest()
                        for area in areas {
                            if alarm.intersects(area: area) {
                                return true
                            }
                        }
                        return false
                    })
                    
                    self.active = alarms.filter({ (alarm) -> Bool in
                        return alarm.active
                    })
                    
                    self.inactive = alarms.filter({ (alarm) -> Bool in
                        return !alarm.active
                    })
                    
                    self.refreshControl?.endRefreshing()
                    self.tableView.reloadData()
                }
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return max(self.active.count, 1)
        }
        return max(self.inactive.count, 1)
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return NSLocalizedString("Aktiva meddelanden", comment: "")
        }
        return NSLocalizedString("Gamla meddelanden", comment: "")
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        struct TimestampDateFormatter {
            static let formatter: DateFormatter = {
                let formatter = DateFormatter()
                formatter.dateFormat = "YYYY-MM-dd HH:mm"
                return formatter
            }()
        }
        
        var cell: UITableViewCell
        
        let alarms = indexPath.section == 0 ? self.active : self.inactive
        
        if alarms.count > indexPath.row {
            cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath)
            
            let alarm = alarms[indexPath.row]
            cell.textLabel?.text = alarm.subject
            
            if let updated = alarm.updated {
                cell.detailTextLabel?.text = TimestampDateFormatter.formatter.string(from: updated)
            }
            
            cell.selectionStyle = .default
            cell.accessoryType = .disclosureIndicator
            
            return cell
        }
        
        cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell", for: indexPath)
        cell.selectionStyle = .none
        cell.accessoryType = .none
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alarms = indexPath.section == 0 ? self.active : self.inactive
        
        guard indexPath.row < alarms.count else {
            // The user tapped the empty cell: do nothing.
            return
        }
        
        let alarm = alarms[indexPath.row]
        if let url = alarm.messageUrl() {
            let controller = SFSafariViewController.init(url: url)
            self.present(controller, animated: true, completion: nil)
        }
    }

}
