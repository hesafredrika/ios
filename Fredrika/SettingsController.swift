//
//  SettingsController.swift
//  Fredrika
//
//  Created by Alexander Simson on 2015-12-22.
//  Copyright © 2015 FR. All rights reserved.
//

import UIKit
import Alamofire

class SettingsController: UITableViewController, SelectLocationControllerDelegate {
    
    var places: Array<AreaOfInterest> = Array<AreaOfInterest>()
    var categories: Array<Volunteering> = Array<Volunteering>()
    var availableCategories: Array<Volunteering> = Array<Volunteering>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backButtonItem: UIBarButtonItem = UIBarButtonItem.init(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil);
        self.navigationItem.backBarButtonItem = backButtonItem;
        
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.estimatedRowHeight = 60.0;
        
        self.places = SettingsStorage.areasOfInterest()
        self.categories = SettingsStorage.volunteeringOptions()
        self.tableView.reloadData()
        
        Alamofire.request("https://hesafredrika.se/api/1.0/volunteers")
            .responseJSON { response in
                if let responseArray = response.result.value as? NSArray {
                    var categories: Array<Volunteering> = Array<Volunteering>()
                    
                    for object in responseArray {
                        if let category = object as? Dictionary<AnyHashable, Any> {
                            let id = category["id"] as! Int
                            let selected = self.categories.filter({ $0.id == id }).count > 0
                            let option = Volunteering(withPayload: category)
                            option.selected = selected
                            categories.append(option)
                        }
                    }
                    
                    self.availableCategories = categories;
                }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.places = SettingsStorage.areasOfInterest()
        self.categories = SettingsStorage.volunteeringOptions()
        self.tableView.reloadData()
    }
    
    // MARK: - Getters
    
    func selectedCategories() -> Array<Volunteering> {
        return self.categories.filter {
            return $0.selected
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 2) {
            return self.places.count + 1
        } else if(section == 3) {
            return self.selectedCategories().count + 1
        }
        return 1
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return NSLocalizedString("Viktiga meddelanden", comment: "")
        } else if section == 1 {
            return NSLocalizedString("GPS-Position", comment: "")
        } else if section == 2 {
            return NSLocalizedString("Områden", comment: "")
        }
        return NSLocalizedString("Ställ upp som frivillig", comment: "")
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if section == 2 {
            return NSLocalizedString("Lägg till områden som du vill få meddelande för. Detta är oberoende av din aktuella GPS-position.", comment: "")
        } else if section == 3 {
            return NSLocalizedString("Om du vill hjälpa till när det behövs lägg då till det du är villig att hjälpa till med.", comment: "")
        }
        return ""
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        
        if indexPath.section == 0 || indexPath.section == 1 {
            cell = tableView.dequeueReusableCell(withIdentifier: "switchCell", for: indexPath)
            let switchCell = cell as! SwitchCell
            if indexPath.section == 0 {
                switchCell.titleLabel.text = NSLocalizedString("Ta emot viktiga meddelanden", comment:"")
                switchCell.descriptionLabel.text = NSLocalizedString("Ta emot viktiga meddelanden från Hesa Fredrika", comment:"")
                switchCell.enableSwitch.setOn(UserDefaults.standard.receiveMessagesEnabled, animated: false)
            } else {
                switchCell.titleLabel.text = NSLocalizedString("Där jag befinner mig", comment:"")
                switchCell.descriptionLabel.text = NSLocalizedString("Ta emot viktiga meddelanden baserat på geografisk position", comment:"")
                switchCell.enableSwitch.setOn(UserDefaults.standard.matchWithCurrentLocationEnabled, animated: false)
            }
            switchCell.enableSwitch.tag = indexPath.section
            switchCell.enableSwitch.addTarget(self, action: #selector(switchValueDidChange(sender:)), for: .valueChanged)
        } else if indexPath.section == 2 {
            if indexPath.row < self.tableView(tableView, numberOfRowsInSection: 2) - 1 {
                cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath)
                let areas = self.places
                let option: AreaOfInterest = areas[indexPath.row]
                cell.textLabel?.text = option.name
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "addCell", for: indexPath)
            }
        } else if indexPath.section == 3  {
            if indexPath.row < self.tableView(tableView, numberOfRowsInSection: 3) - 1 {
                cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath)
                let categories = self.selectedCategories()
                let option: Volunteering = categories[indexPath.row]
                cell.textLabel?.text = option.name
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "addCell", for: indexPath)
            }
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 2 {
            return indexPath.row < self.tableView(tableView, numberOfRowsInSection: 2) - 1;
        } else if indexPath.section == 3 {
            return indexPath.row < self.tableView(tableView, numberOfRowsInSection: 3) - 1;
        }
        return false
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if indexPath.section == 2 {
                let area = self.places[indexPath.row]
                SettingsStorage.remove(areaOfInterest: area)
                self.places = SettingsStorage.areasOfInterest()
                tableView.deleteRows(at: [indexPath], with: .fade)
            } else {
                let volunteering = self.categories[indexPath.row]
                SettingsStorage.remove(volunteering: volunteering)
                self.categories = SettingsStorage.volunteeringOptions()
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 3) {
            let storyboard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            let options: OptionsController = storyboard.instantiateViewController(withIdentifier: "select") as! OptionsController
            options.title = NSLocalizedString("Välj hjälp-område", comment: "")
            options.objects = self.availableCategories
            self.navigationController?.pushViewController(options, animated: true)
        } else if(indexPath.section == 2) {
            let storyboard: UIStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            let location: SelectLocationController = storyboard.instantiateViewController(withIdentifier: "location") as! SelectLocationController
            location.delegate = self
            location.title = NSLocalizedString("Välj område", comment: "")
            
            let navigationController = UINavigationController.init(rootViewController: location)
            self.navigationController?.present(navigationController, animated: true, completion: nil)
        }
    }
    
    // MARK: - SelectLocationControllerDelegate

    func didSelect(area: AreaOfInterest, controller: SelectLocationController) {
        controller.dismiss(animated: true)
    }
    
    // MARK: - Actions
    
    func switchValueDidChange(sender: UISwitch!) {
        if sender.tag == 0 {
            UserDefaults.standard.receiveMessagesEnabled = sender.isOn
        } else if sender.tag == 1 {
            UserDefaults.standard.matchWithCurrentLocationEnabled = sender.isOn
        }
    }
}

