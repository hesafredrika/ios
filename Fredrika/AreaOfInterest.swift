//
//  AreaOfInterest.swift
//  Fredrika
//
//  Created by Alexander Simson on 2016-12-09.
//  Copyright © 2016 FR. All rights reserved.
//

import UIKit
import CoreLocation

class AreaOfInterest: NSObject, NSCoding {

    var location : CLLocation?
    var radius : Float = 2000 // Default to 2 km
    var name : String?
    
    required init(withLocation location: CLLocation, radius: Float, name: String) {
        self.location = location
        self.radius = radius
        self.name = name
    }
    
    required init(coder decoder: NSCoder) {
        self.location = decoder.decodeObject(forKey: "location") as? CLLocation
        self.radius = decoder.decodeFloat(forKey: "radius")
        self.name = decoder.decodeObject(forKey: "name") as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(location, forKey: "location")
        coder.encode(radius, forKey: "radius")
        coder.encode(name, forKey: "name")
    }
    
    static func == (lhs: AreaOfInterest, rhs: AreaOfInterest) -> Bool {
        return (lhs.name == rhs.name) && (lhs.radius == rhs.radius)
    }
    
    override var debugDescription : String {
        return "Area Of Interest: \(String(describing: self.name)), radius: \(self.radius)"
    }
    
}
